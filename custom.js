jQuery(document).ready(function () {

    // validation and enable/disable section
    jQuery('#cavex_calc input, #cavex_calc select').bind('input propertychange', function() {
        // Declare variables
        let flowrate, cyclone_size, operating_pressure, cyclones_percentage, terms_condition;
        // Access form values
        jQuery('input, select').each(function(index){  
            if(jQuery(this).prop('name') === 'flowrate'){
                flowrate = jQuery(this).val();
            }
            if(jQuery(this).prop('name') === 'cyclone_size'){
                cyclone_size = jQuery(this).val();
            }
            if(jQuery(this).prop('name') === 'operating_pressure'){
                operating_pressure = jQuery(this).val();
            }
            if(jQuery(this).prop('name') === 'cyclones_percentage'){
                cyclones_percentage = jQuery(this).val();
            }
            if(jQuery(this).prop('name') === 'terms_condition'){
                console.lo
                if(jQuery(this).is(":checked")){
                    terms_condition = jQuery(this).val();
                }
            }
        });
        
        // Drop down enabled and disabled based on text box values
        if(flowrate === '') {
            jQuery("#cyclone_size, #operating_pressure, #cyclones_percentage").val('');
            jQuery("#cyclone_size, #operating_pressure").prop('disabled', true);
        } else{
            jQuery("#cyclone_size, #operating_pressure").prop('disabled', false);
        }

        // Terms & Submit button enable/disable based on all inputs
        if(flowrate !== '' && cyclone_size !== '' && operating_pressure !== '' && terms_condition ==='1'){
            jQuery("#calculate").prop('disabled', false);
        } else {
            jQuery("#calculate").prop('disabled', true);
        }
    });

    jQuery('#calculate').click(function(){
        jQuery("#calculate").prop('disabled', true);
        cavexCalculation();
    });

    //Bootstrap responsive table
    jQuery('.table-responsive-stack').each(function (i) {
        var id = jQuery(this).attr('id');
        //alert(id);
        jQuery(this).find("th").each(function(i) {
            jQuery('#'+id + ' td:nth-child(' + (i + 1) + ')').prepend('<span class="table-responsive-stack-thead">'+jQuery(this).text() + ':</span> ');
            jQuery('.table-responsive-stack-thead').hide();
            
        });
    });
       
    jQuery( '.table-responsive-stack' ).each(function() {
      var thCount = jQuery(this).find("th").length; 
       var rowGrow = 100 / thCount + '%';
       jQuery(this).find("th, td").css('flex-basis', rowGrow);   
    });
       
    function flexTable(){
       if (jQuery(window).width() < 768) {
        jQuery(".table-responsive-stack").each(function (i) {
            jQuery(this).find(".table-responsive-stack-thead").show();
            jQuery(this).find('thead').hide();
        });
       // window is less than 768px   
       } else {
        jQuery(".table-responsive-stack").each(function (i) {
            jQuery(this).find(".table-responsive-stack-thead").hide();
            jQuery(this).find('thead').show();
        });
       }
        // flextable   
    }      
     
    flexTable();
    window.onresize = function(event) {
        flexTable();
    };
});

function cavexCalculation(){
    const calculatorForm = jQuery("#cavex_calc");
    jQuery.ajax({
        dataType: 'json',
        url: admin_ajax_url,
        type: "POST",
        data:  calculatorForm.serialize(),
        success: function(response) {
            jQuery("#calculate").prop('disabled', false);
            jQuery(".ajaxResult").removeClass('d-none');
            jQuery("#cavex_calc").addClass('d-none');
            console.log(response);
            if(response.error) {
                jQuery(".errorResponse").html("<h4 class='p-3'>"+response.message+"</h4>");
            }else if(response.success) {
                jQuery(".successResponse").html(response.result);
            }
        },
        error: function(data) {
            jQuery(".ajaxResult").addClass('d-none');
            jQuery("#cavex_calc").removeClass('d-none');
        }
    });
}