<?php
	 /**
	 * function primary  menu array list
	 * @param array NULL
	 * @return array
	 */
	function register_townsquare_menu() {
	  register_nav_menu('townsquare-menu',__( 'townsquare Menu' ));
	}

	add_action( 'init', 'register_townsquare_menu' );		

	/**
		* Function to add site setting option page
		* @param Null
		* @return Generate site setting page on admin panel
	*/	
	if( function_exists('acf_add_options_page') ) {
		acf_add_options_page(array(
			'page_title' 	=> 'Messages Settings',
			'menu_title'	=> 'CVX/CVD Settings',
			'menu_slug' 	=> 'cvx-cvd-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));

		if( function_exists('acf_add_options_sub_page') ) {
	        
	        acf_add_options_sub_page(array(
	            'page_title'  => __('Prices'),
	            'menu_title'  => __('Prices'),
	            'parent_slug' => 'cvx-cvd-settings',
            ));
            
	        acf_add_options_sub_page(array(
	            'page_title'  => __('Capacity'),
	            'menu_title'  => __('Capacity'),
	            'parent_slug' => 'cvx-cvd-settings',
            ));
            
	        acf_add_options_sub_page(array(
	            'page_title'  => __('Spare Cyclone'),
	            'menu_title'  => __('Spare Cyclone'),
	            'parent_slug' => 'cvx-cvd-settings',
	        ));
	    }
	}	

	/**
	* The purpose of the function is return all the articles for the collection. 
	*/
	add_action( 'wp_ajax_nopriv_cavex_calculator',  'cavex_calculator' );
	add_action( 'wp_ajax_cavex_calculator','cavex_calculator' );
	function cavex_calculator() {

		$data = $_POST;
		$flowrate = ($data['flowrate']) ? $data['flowrate'] : '';
		$cyclone_size = ($data['cyclone_size']) ? $data['cyclone_size'] : '';
		$operating_pressure = ($data['operating_pressure']) ? $data['operating_pressure'] : '';
		$cyclones_percentage = ($data['cyclones_percentage']) ? $data['cyclones_percentage'] : '';
		$terms_condition = ($data['terms_condition']) ? $data['terms_condition'] : '';

		$cvx_capacity_value = 0;
		$cvd_capacity_value = 0;
		$cvx_heading = '';
		$cvd_heading = '';
		$cvxPricePerCylendar = 0;
		$cvdPricePerCylendar = 0;
		$output = '';

		if($flowrate == '' || $cyclone_size == '' || $operating_pressure == '' || $terms_condition == ''){
			errorResponse("Tressparsing not allowed");
		} else {

			//Calculation
			if ( have_rows( $cyclone_size, 'option' ) ) : 
				while ( have_rows( $cyclone_size, 'option' ) ) : the_row(); 
					if ( have_rows( 'capacity_info' ) ) : 
						while ( have_rows( 'capacity_info' ) ) : the_row(); 
							if($operating_pressure === get_sub_field( 'pressure_kpa')) {
								$cvx_capacity_value = get_sub_field( 'cvx' );
								$cvd_capacity_value = get_sub_field( 'cvd' );
							}
						endwhile; 
					else : 
						errorResponse('capacity info not found');
					endif; 
					$cvx_heading = get_sub_field( 'cvx_heading' );
					$cvd_heading = get_sub_field( 'cvd_heading' ); 
				endwhile; 
			endif; 

			$numberOfCyclonOperatingCVX = round($flowrate/$cvx_capacity_value);
			$numberOfCyclonOperatingCVD = round($flowrate/$cvd_capacity_value);

			if($cyclones_percentage && $cyclones_percentage !='') {
				
				$spareCycloneCvx = ceil(($numberOfCyclonOperatingCVX*$cyclones_percentage) / 100);
				$spareCycloneCvd = ceil(($numberOfCyclonOperatingCVD*$cyclones_percentage) / 100);
			}
			$cylenderRequiredCVX = $numberOfCyclonOperatingCVX + $spareCycloneCvx;
			$cylenderRequiredCVD = $numberOfCyclonOperatingCVD + $spareCycloneCvd;

			// Prices as per cyclone size
			if ( have_rows( 'cavex_prices', 'option' ) ) : 
				while ( have_rows( 'cavex_prices', 'option' ) ) : the_row(); 
				if($cyclone_size === get_sub_field( 'cyclone_sizes' )) {
					$cvxPricePerCylendar = get_sub_field( 'cvx_price' ); 
					$cvdPricePerCylendar = get_sub_field( 'cvd_price' ); 
				}
				endwhile; 
			endif;
			$totalCvxPrice =  $cvxPricePerCylendar * $cylenderRequiredCVX;
			$totalCvdPrice =  $cvdPricePerCylendar * $cylenderRequiredCVD;
			setlocale(LC_MONETARY, 'en_US');
			$finalCVXPrice = money_format('%.2n', $totalCvxPrice);
			$finalCVDPrice = money_format('%.2n', $totalCvdPrice);
			$output .='<table class="table borderless d-none d-sm-block"> <thead> <tr> <th></th> <th scope="col"> <h3 class="d-inline-flex align-items-center"> Original Cavex <span class="f-3rem">&#174;</span> </h3> </th> <th scope="col"> <h3 class="d-inline-flex align-items-center"> Cavex <span class="f-3rem">&#174;</span> 2 </h3> </th> </tr></thead> <tbody> <tr> <th scope="row" class="w-20">Quantity</th> <td class="w-40">'.$cylenderRequiredCVX.'</td><td class="w-40">'.$cylenderRequiredCVD.'</td></tr><tr> <th scope="row" class="w-20">Description</th> <td class="w-40"><p>'.$cvx_heading.'</p></td><td class="w-40"><p>'.$cvd_heading.'</p></td></tr><tr> <th scope="row" class="w-20"> Extended Price<br/> (Weir Dollars) </th> <td class="w-40"><b>'.$finalCVXPrice.'</b></td><td class="w-40"><b>'.$finalCVDPrice.'</b></td></tr></tbody></table><table class="table borderless table-responsive-stack d-sm-none" id="tableOne"> <tbody> <tr> <th scope="row"> <h3 class="d-inline-flex align-items-center"> Original Cavex <span class="f-3rem">&#174;</span> </h3> </th> </tr><tr> <th scope="row">Quantity</th> <td >'.$cylenderRequiredCVX.'</td></tr><tr> <th scope="row">Description</th> <td><p>'.$cvx_heading.'</p></td></tr><tr> <th scope="row"> Extended Price<br/> (Weir Dollars) </th> <td><b>'.$finalCVXPrice.'</b></td></tr><tr> <th scope="row"> <hr> <h3 class="d-inline-flex align-items-center"> Cavex <span class="f-3rem">&#174;</span> 2 </h3> </th> </tr><tr> <th scope="row">Quantity</th> <td>'.$cylenderRequiredCVD.'</td></tr><tr> <th scope="row">Description</th> <td><p>'.$cvd_heading.'</p></td></tr><tr> <th scope="row"> Extended Price<br/> (Weir Dollars) </th> <td><b>'.$finalCVDPrice.'</b></td></tr></tbody> </table>';
			$successData['status'] = '200';
			$successData['success'] = true;
			$successData['result'] = $output; 
			
			echo json_encode($successData); 
			wp_die();
		}
	}

	function errorResponse($msg) {
		$data['status'] = '200';
		$data['error'] = true;
		$data['message'] = $msg; 
		echo json_encode($data);
		wp_die();
	}
