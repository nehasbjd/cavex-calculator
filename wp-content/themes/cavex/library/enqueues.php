<?php
	/**!
	 * Enqueues
	 */
	
	//enqueues style and scripts
	if ( ! function_exists('tsa_enqueues') ) {
		
		add_action('wp_enqueue_scripts', 'tsa_enqueues', 100);
		
		function tsa_enqueues() {

			// Gogole fonts and bootstrap styles
			wp_register_style('tsa-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css', false, null);
			wp_enqueue_style('tsa-bootstrap');
					
			//MAIN CVX/CVD CSS
			$townsquaretime = filemtime( get_stylesheet_directory() . '/assets/css/style.css' );
			wp_register_style('tsa-parent-style', get_stylesheet_directory_uri() . '/assets/css/style.css', 
			false, // dependencies
			$townsquaretime, // version number
			null // load in footer
			);
			wp_enqueue_style('tsa-parent-style');	
			
		
			//Bootstrap library Scripts
			wp_register_script('tsa-jmin.min', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js', false, null, null);
			wp_enqueue_script('tsa-jmin.min');
			
			wp_register_script('tsa-bootstrap.min', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', false, null, null);
			wp_enqueue_script('tsa-bootstrap.min');
			
			wp_register_script('tsa-validate.min', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.js', false, null, null);
			wp_enqueue_script('tsa-validate.min');
			
			
			//townsquare MAIN SCRIPT					
			$jcustom = filemtime( get_stylesheet_directory() . '/assets/js/custom.js' );
			wp_register_script('tsa-custom', get_stylesheet_directory_uri().'/assets/js/custom.js', 
			false, // dependencies
			$jcustom, // version number
			null // load in footer
			);
			wp_enqueue_script('tsa-custom');
		
			
		}
	}

	