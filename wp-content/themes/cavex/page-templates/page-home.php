<?php
/**
* Template Name: CVX/CVD Calculator
*
* @package WordPress
* @subpackage Twenty_Twenty
* @since Twenty Twenty 1.0
*/
get_header();
$background = ($_GET['background']) ? $_GET['background'] : '';
?>

<div class="wrap container yellowBG" style="background:<?php echo "#".$background; ?>" role="document">
    <div class="content">
        <main class="main">
			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content', 'calc' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
			
		</main><!-- #main -->
	</div>
</div>			
<?php
get_footer();
