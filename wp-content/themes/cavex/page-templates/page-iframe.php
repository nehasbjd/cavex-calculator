<?php
/**
* Template Name: Render Iframe
*
* @package WordPress
* @subpackage Twenty_Twenty
* @since Twenty Twenty 1.0
*/
get_header();
?>

<div class="wrap container" role="document">
    <div class="content">
        <main class="main">
		<h3 class="pt-3 pb-3">Cavex Calculator in Iframe</h3>
		<div class="tsa-cavex-calc"></div>

		<script type="text/javascript" src="https://cavex-staging.townsquareagency.com.au/wp-content/themes/cavex/js/cavex_cvd_calculator.js"></script>
		<script>
			myCavex.generateCavexIframe({
				target: "tsa-cavex-calc",
				selectorType: "class",
				width: "100%",
				height: "600px",
				backgroundColor: "FBD53B" // Please use color code without #
			});
		</script>
			
		</main><!-- #main -->
	</div>
</div>			
<?php
//get_footer();
 