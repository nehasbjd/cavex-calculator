<?php
/**
* Template part for displaying page content in page.php
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package cavex
*/
?>

<section class='cavex-container'>
    <div class='row'>
        <div class='col-lg-12'>
            <div class='page-header'>
                <h1><?php the_field( 'form_heading', 'option' ); ?></h1>
            </div>
            <p><?php the_field( 'form_description', 'option' ); ?></p>
        </div>
        <div class='col-lg-12'>
            <form id="cavex_calc" class="calc pt-4">
                <input type="hidden" name="action" value="cavex_calculator"/>
                <div class="row">
                    <div class="col-md-7 gap">
                        
                        <!-- Flowrate Field -->
                        <div class="form-field">
                            <label class="frm_primary_label">
                                <h3>Flowrate, I/s <span class="frm_required">*</span></h3>
                                <div class="frm_description">
                                    This is an explanation line to help understand what to input.
                                </div>
                            </label>
                            <input name="flowrate" id="flowrate" type="number" placeholder="Type flowrate here" min="1" max="1200" step="1">
                        </div>

                        <!-- Cyclone Size Field -->
                        <div class="form-field">
                            <label class="frm_primary_label">
                                <h3>Cyclone Size <span class="frm_required">*</span></h3>
                                <div class="frm_description">
                                    This is an explanation line to help understand what to input.
                                </div>
                            </label>
                            <select id="cyclone_size" name="cyclone_size" placeholder="Select cyclone size" disabled>
                                <option value=""> Select cyclone size </option>
                                <?php 
                                if ( have_rows( 'cavex_prices', 'option' ) ) :
                                    while ( have_rows( 'cavex_prices', 'option' ) ) : the_row(); ?>
                                        <option value="<?php echo get_sub_field( 'cyclone_sizes' ); ?>" class=""><?php echo get_sub_field( 'cyclone_sizes' ); ?></option>
                                    <?php 
                                    endwhile;
                                endif; 
                                ?>
                            </select>
                        </div>

                        <!-- Operating Pressure, kPa Field -->		
                        <div class="form-field">
                            <label class="frm_primary_label">
                                <h3>Operating Pressure, kPa <span class="frm_required">*</span></h3>
                                <div class="frm_description">
                                    This is an explanation line to help understand what to input.
                                </div>
                            </label>
                            <select id="operating_pressure" name="operating_pressure" placeholder="Select operating pressure" disabled>
                                <option value=""> Select operating pressure </option>
                                <?php 
                                if ( have_rows( 'pressure_kpa_dropdown', 'option' ) ) :
                                    while ( have_rows( 'pressure_kpa_dropdown', 'option' ) ) : the_row(); ?>
                                        <option value="<?php echo get_sub_field( 'pressure_kpa_dd' ); ?>" class=""><?php echo get_sub_field( 'pressure_kpa_dd' ); ?></option>
                                    <?php 
                                    endwhile;
                                endif; 
                                ?>
                            </select>
                        </div>

                        <!-- Spare cyclones Field -->        
                        <div class="form-field">
                            <label class="frm_primary_label">
                                <h3>Include any spare cyclones in your calculation? </h3>
                                <div class="frm_description">
                                    This is an explanation line to help understand what to input.
                                </div>
                            </label>
                            <select id="cyclones_percentage" name="cyclones_percentage" placeholder="Select operating pressure">
                                <option value=""> Select percentage </option>
                                <?php 
                                if ( have_rows( 'spare_cyclones_percentage', 'option' ) ) :
                                    while ( have_rows( 'spare_cyclones_percentage', 'option' ) ) : the_row(); ?>
                                        <option value="<?php echo get_sub_field( 'spare_cyclones' ); ?>" class=""><?php echo get_sub_field( 'spare_cyclones' ); ?>%</option>
                                    <?php 
                                    endwhile;
                                endif; 
                                ?>
                            </select>
                        </div>
                    
                        <!-- Terms & Condition -->        
                        <div class="d-flex flex-wrap align-items-center">
                            <div class="terms_condition vertical_radio mb-4">
                                <div class="frm_opt_container">
                                    <div class="frm_checkbox">
                                        <label for="terms_condition">
                                        <input id="terms_condition" name="terms_condition" type="checkbox" value="1"> I agree to the terms &amp; conditions</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Submit Button -->
                        <div class="form-field">
                            <div class="calculation_btn">
                                <button class="btn btn-submit" id="calculate" disabled type="button">Submit</button>
                            </div>
                        </div>        
                        <!-- closing first column -->
                    </div>
                </div>
            </form>    

            <!-- Results container -->
            <div class="row ajaxResult d-none">
                <div class='col-lg-12'>
                    <div class="results slide-top px-3">
                        <div class="errorResponse"></div>
                        <div class="successResponse"></div>
                        <div class="disclaimer py-sm-3 px-sm-5">
                            <?php the_field( 'disclaimer', 'option' ); ?>
                        </div>    
                    </div>   
                    <div class="w-100 text-right mt-3">
                        <button onClick="window.location.reload();" class="btn btn-submit" type="button">Recalculate</button>
                    </div> 
                </div>
            </div>
            <!-- Results container Ends -->
        </div>
    </div>

</section>