<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cavex
 */

?>

	<footer id="colophon" class="site-footer text-center mt-2">
		<div class="site-info">
			<a class="no-line" target="_blank" href="<?php echo esc_url( __( 'https://townsquare.agency', 'cavex' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'cavex' ), 'Town Square Agency' );
				?>
			</a>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
