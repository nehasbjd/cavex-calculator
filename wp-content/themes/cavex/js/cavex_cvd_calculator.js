class createCvxCvdCalc {
	target;
	cssSelectorType = 'id';
	height = '100%';
	width = '600px';
	backgroundColor = '';
	constructor() {}
	generateCavexIframe(requestedParams) {
		this.target = (requestedParams.target) ? requestedParams.target : this.target;
		this.cssSelectorType = (requestedParams.selectorType) ? requestedParams.selectorType : this.cssSelectorType;
		this.height = (requestedParams.height) ? requestedParams.height : this.height;
		this.width = (requestedParams.width) ? requestedParams.width : this.width;
		this.background = (requestedParams.backgroundColor) ? requestedParams.backgroundColor : this.backgroundColor;
		
		if(this.cssSelectorType === 'id') {
		
			document.getElementById(this.target).innerHTML = "<iframe style='width:"+this.width+";height:"+this.height+"' src='https://cavex-staging.townsquareagency.com.au?background="+this.background+"'></iframe>";
		
		} else if(this.cssSelectorType === 'class') {
		
			document.getElementsByClassName(this.target)[0].innerHTML =  "<iframe style='width:"+this.width+";height:"+this.height+"' src='https://cavex-staging.townsquareagency.com.au?background="+this.background+"'></iframe>";

		}
	} 
} 
let myCavex = new createCvxCvdCalc();
