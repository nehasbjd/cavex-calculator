<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'cavex_webapp_v2' );

/** MySQL database username */
define( 'DB_USER', 'cavex_web_app' );

/** MySQL database password */
define( 'DB_PASSWORD', '5WJ5d8SmdWXu' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ghG6[Q`+Dc]KG;KxH:vR81MxB3lmPYEeB`r/VSaN:4T&v93**4Mgf2xog<kCaZ`>' );
define( 'SECURE_AUTH_KEY',  '[45#a|Pc8cKEVUpe5`@1fE!}X(OUn|ThDeeoNBKpaL#bq]OyC,-jo7]^v*Wos~tV' );
define( 'LOGGED_IN_KEY',    'nCD/n(-Oy?o:?`h8C9xHi/OTo7:Gh5sr#?^c |,5$q>R1&d|whWKnpqx=)2]~4ae' );
define( 'NONCE_KEY',        'hw86HVc w}hg)Udnc;|4^82RQOqRIrl_Z&xmqt~FeDaQ8oj=GxtwKY{3oe49}52J' );
define( 'AUTH_SALT',        'T+jbD,/_qD+bsICCku*SAY[{J.HGJMtL!4!ko$D2Ns*:_b^mtx%0YQCitk+WIPYs' );
define( 'SECURE_AUTH_SALT', 'Z8w YsAEE)Ce>L6l9u31<d]XBw-42s4<tcJ1(wAHy$XzHT(J_g.On`Sh2wZkM2,;' );
define( 'LOGGED_IN_SALT',   'LjE7W kx2qN+[.e:[Rc+5C@c3wS1imo1f0./8nZfSr9>JWa./{:&gk6n08Xh`v>O' );
define( 'NONCE_SALT',       'Te_Hi$m}2}WdlJe4/vwqhDBnUZ&sN 9^| .M=:^.9>bvY;VVUz)|L@4-/3vCWrK|' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'cavex_iframe_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
