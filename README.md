## How to deploy Cavex Calculator wordpress setup

First of all, get all code using git clone.

1. Import database tables from the **cavex_webapp_v2.sql.zip** file which is located on root level on clonned code.
2. Once database get imported then change home_url and site_url option from wpprefix_options table.
3. That's it for database changes.
4. Now, Change the DB Crdentials in **wp-config.php** file.
5. Congrats !! You're done with wordpres setup!!


---

## Prices and Form Values Settings

Next, you’ll need to login with admin panel using **https://yourdomain.com/wp-admin**

**Username** : alison

**Password** : townsquareagency@1729!@#


You can change it later from edit profile section.

1. Once you logged in with admin crdentials then visit **CVX/CVD Settings** from left navigations 
2. Here you can see Prices, Capacity and Spare Cyclone sub navigation, you can update the data from here.

## Cavex calculator usage via script

Below is the script, You can use anywhere this script to display Cavex CVX/CVD calculator.

```
<!-- Cavex calculator script starts-->
<div class="tsa-cavex-calc"></div>
<script type="text/javascript" src="https://cavex-staging.townsquareagency.com.au/wp-content/themes/cavex/js/cavex_cvd_calculator.js"></script>
<script>
myCavex.generateCavexIframe({ 
    target: "tsa-cavex-calc", // Selector where iframe would be render
    selectorType: "class", // Selector type either id or class
    width: "100%",  // iframe width
    height: "600px", // iframe height
    backgroundColor: "FBD53B" // background color of calculator, Please use color code without #            
});
</script>
<!-- Cavex calculator script Ends-->
```